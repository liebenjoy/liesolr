package com.lieben.solr.poc.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lieben.solr.poc.service.AccountIndexingService;
import com.lieben.solr.poc.service.AccountIndexingServiceImpl;

public class AccountIndexingServlet extends HttpServlet {
	
	private AccountIndexingService accountIndexingService;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doPost(req, resp);
		accountIndexingService.fullIndexAccounts();
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		accountIndexingService = new AccountIndexingServiceImpl();
	}
	
}
