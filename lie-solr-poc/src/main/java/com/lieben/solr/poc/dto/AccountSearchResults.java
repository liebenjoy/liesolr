package com.lieben.solr.poc.dto;

import java.util.List;

import com.lieben.solr.poc.dao.AccountDocument;

public class AccountSearchResults {
	
	private List<AccountDocument> accounts;

	public List<AccountDocument> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<AccountDocument> accounts) {
		this.accounts = accounts;
	}
	

}
