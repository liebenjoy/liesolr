package com.lieben.solr.poc.service;

import java.sql.Connection;
import java.util.List;

import com.lieben.solr.poc.dao.AccountDocument;

public interface AccountService {

	public List<AccountDocument> getAllAccounts();
	
}
