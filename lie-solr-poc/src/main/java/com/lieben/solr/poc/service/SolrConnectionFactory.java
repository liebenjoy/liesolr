package com.lieben.solr.poc.service;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

public class SolrConnectionFactory {
	
	private static final String CLOUD_SOLR_SERVER_ZOOKEEPER_URL = "localhost:9983";
	
	private static final String STANDALONE_SOLR_SERVER_URL = "http://localhost:8983/solr";
	
	public static SolrClient getSolrClient() {
		//SolrClient solrClient = new CloudSolrClient(CLOUD_SOLR_SERVER_ZOOKEEPER_URL);
		SolrClient solrClient = new HttpSolrClient(STANDALONE_SOLR_SERVER_URL);
		return solrClient;
	}

	

}
