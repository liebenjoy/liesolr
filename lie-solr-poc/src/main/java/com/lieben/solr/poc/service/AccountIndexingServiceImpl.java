package com.lieben.solr.poc.service;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

import com.lieben.solr.poc.dao.AccountDocument;

public class AccountIndexingServiceImpl implements AccountIndexingService {
	
	private AccountService accountService;
	
	public AccountIndexingServiceImpl() {
		this.accountService = new AccountServiceImpl();
	}
	
	public void fullIndexAccounts() {
		List<AccountDocument> accounts = accountService.getAllAccounts();
		if(accounts != null && !accounts.isEmpty()) {
			try {
				SolrClient solrClient = SolrConnectionFactory.getSolrClient();
				for(int i = 0; i < accounts.size(); i = i + 1000) {
					int lastIndex = i + 1000;
					if((i + 1000) >= accounts.size()) {
						lastIndex = accounts.size() - 1;
					}
					System.err.println("from = " + i + ", to = " + lastIndex);
					List<AccountDocument> subList = accounts.subList(i, lastIndex);
					solrClient.addBeans("liebenAccounts", subList);
					solrClient.commit("liebenAccounts");
				}
			} catch (SolrServerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
