package com.lieben.solr.poc.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.lieben.solr.poc.dao.AccountDocument;
import com.lieben.solr.poc.dto.AccountSearchResults;
import com.lieben.solr.poc.service.AccountSearchService;
import com.lieben.solr.poc.service.AccountSearchServiceImpl;

public class AccountSearchServlet extends HttpServlet {
	
	private AccountSearchService accountSearchService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String searchTerm = req.getParameter("searchTerm");
		List<AccountDocument> accounts = accountSearchService.searchAccounts(searchTerm);
		AccountSearchResults searchResults = new AccountSearchResults();
		searchResults.setAccounts(accounts);
		String jsonString = getJSONString(searchResults);
		resp.setContentType("application/json");
		resp.getWriter().print(jsonString);
		resp.flushBuffer();
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.accountSearchService = new AccountSearchServiceImpl();
	}
	
	private String getJSONString(AccountSearchResults accountSearchResults) {
		String jsonString = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			// convert user object to json string, and save to a file
			jsonString = mapper.writeValueAsString(accountSearchResults);
			// display to console
			System.out.println(jsonString);
	 
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonString;
	}

}
