package com.lieben.solr.poc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnectionFactory {
	
	private static final String DBURL = "jdbc:mysql://localhost:3306/test";
	
	private static final String DBUSER = "root";
	
	private static final String DBPASSWORD = "Admin123";
	
	private static final String DBDRIVERCLASSNAME = "com.mysql.jdbc.Driver";
	
	public static Connection getConnection() {
		try {
			Class.forName(DBDRIVERCLASSNAME);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection = null;
	    Properties connectionProps = new Properties();
	    connectionProps.put("user", DBUSER);
	    connectionProps.put("password", DBPASSWORD);
        try {
			connection = DriverManager.getConnection(DBURL, connectionProps);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return connection;
    }

}
