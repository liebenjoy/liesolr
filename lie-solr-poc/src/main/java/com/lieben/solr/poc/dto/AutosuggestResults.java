package com.lieben.solr.poc.dto;

import java.util.List;

public class AutosuggestResults {
	
	private List<String> suggestions;

	public List<String> getSuggestions() {
		return suggestions;
	}

	public void setSuggestions(List<String> suggestions) {
		this.suggestions = suggestions;
	}

}
