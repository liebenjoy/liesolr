package com.lieben.solr.poc.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse.Suggestion;

import com.lieben.solr.poc.dao.AccountDocument;

public class AccountSearchServiceImpl implements AccountSearchService {
	
	public List<AccountDocument> searchAccounts(String searchTerm) {
		if(searchTerm == null) {
			searchTerm = "*";
		} else if(!searchTerm.endsWith("*")) {
			searchTerm = searchTerm.concat("*");
		}
		List<AccountDocument> accounts = null;
		SolrClient solrClient = SolrConnectionFactory.getSolrClient();
		SolrQuery query = new SolrQuery();
	    query.setQuery( "text:" + searchTerm);
	    query.addSort("accountAmount", SolrQuery.ORDER.desc );
		try {
			QueryResponse queryResponse = solrClient.query("liebenAccounts", query);
			if(null != queryResponse) {
				accounts = queryResponse.getBeans(AccountDocument.class);
			}
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return accounts;
	}
	
	public List<String> autosuggest(String searchTerm) {
		if(searchTerm == null) {
			throw new IllegalArgumentException("Search Term cannot be null");
		}
		List<String> suggestionStrings = new ArrayList<String>();
		SolrClient solrClient = SolrConnectionFactory.getSolrClient();
		SolrQuery query = new SolrQuery(searchTerm.toLowerCase());
		query.setRequestHandler("/suggest");
	    query.addSort("accountAmount", SolrQuery.ORDER.desc );
		try {
			QueryResponse queryResponse = solrClient.query("liebenAccounts",  query);
			List<Suggestion> suggestions = queryResponse.getSpellCheckResponse().getSuggestions();
			if(null != suggestions) {
				for(Suggestion suggestion : suggestions) {
					if(null != suggestion.getAlternatives()) {
						suggestionStrings.addAll(suggestion.getAlternatives());
					}
				}
			}
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return suggestionStrings;
	}

}
