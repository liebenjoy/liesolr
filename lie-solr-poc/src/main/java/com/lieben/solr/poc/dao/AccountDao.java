package com.lieben.solr.poc.dao;

import java.util.List;

public interface AccountDao {
	
	public List<AccountDocument> getAllAccounts();

}
