package com.lieben.solr.poc.service;

import java.util.List;

import com.lieben.solr.poc.dao.AccountDocument;

public interface AccountSearchService {
	
	public List<AccountDocument> searchAccounts(String searchTerm);
	
	public List<String> autosuggest(String searchTerm);

}
