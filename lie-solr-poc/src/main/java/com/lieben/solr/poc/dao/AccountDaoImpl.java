package com.lieben.solr.poc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountDaoImpl implements AccountDao {
	
	private static final String ACCOUNT_A_QUERY = "select acct_a.ID as ACCOUNT_ID, acct_a.NAME as ACCOUNT_NAME, acct_a.AMOUNT as ACCOUNT_AMOUNT, acct_a.CREATION_DATE as ACCOUNT_CREATE_DATE from lieben_account_type_a acct_a";
	
	private static final String USER_DETAILS_QUERY = "select lieben_user.ID AS USER_ID, lieben_user.FIRST_NAME as USER_FIRST_NAME, lieben_user.LAST_NAME as USER_LAST_NAME from lieben_user lieben_user";
	
	private static final String ACCOUNT_A_ACCESS_QUERY = "select ACCT_A_ACCESS.ACCOUNT_A_ID as ACCOUNT_A_ID, lieben_user.ID AS USER_ID, lieben_user.FIRST_NAME as USER_FIRST_NAME, lieben_user.LAST_NAME as USER_LAST_NAME from lieben_user_account_a_access ACCT_A_ACCESS join lieben_user lieben_user on lieben_user.ID = ACCT_A_ACCESS.USER_ID";
	
	
	public List<AccountDocument> getAllAccounts() {
		Connection connection = DBConnectionFactory.getConnection();
		List<AccountDocument> accounts = new ArrayList<AccountDocument>();
        PreparedStatement stmt = null;
        
        try {
            stmt = connection.prepareStatement(ACCOUNT_A_QUERY);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
            	AccountDocument accountDoc = new AccountDocument();
	            accountDoc.setAccountId(rs.getLong("ACCOUNT_ID"));
	            accountDoc.setAccountName(rs.getString("ACCOUNT_NAME"));
	            accountDoc.setAccountAmount(rs.getDouble("ACCOUNT_AMOUNT"));
	            accountDoc.setAccountCreationDate(rs.getDate("ACCOUNT_CREATE_DATE"));
	            accounts.add(accountDoc);
            }
        } catch (SQLException e) {
        	e.printStackTrace();
            if (connection != null) {
                try {
                    System.err.print("Transaction is being rolled back");
                    connection.rollback();
                } catch (SQLException excep) {
                }
            }
        } finally {
            if (stmt != null) {
                try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
        return accounts;
    }

}
