package com.lieben.solr.poc.service;

import java.util.List;

import com.lieben.solr.poc.dao.AccountDao;
import com.lieben.solr.poc.dao.AccountDaoImpl;
import com.lieben.solr.poc.dao.AccountDocument;

public class AccountServiceImpl implements AccountService {
	
	private AccountDao accountDao;
	
	public AccountServiceImpl() {
		this.accountDao = new AccountDaoImpl();
	}
	
	public List<AccountDocument> getAllAccounts() {
		List<AccountDocument> accounts = accountDao.getAllAccounts();
		return accounts;
	}

}
