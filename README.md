# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###


*    A POC on solr
*    1.0


### How do I get set up? ###


*    solr-poc is a maven project
*    Open eclipse > Open Perspective > Git
*   Clone Git Repository > Clone URI > Give the URI of ur git repository, username and password
*    Once checkedout, right click Import Maven projects into workspace
*    database folder has database setup scripts
*    solrStandaloneHome is the solr.solr.home folder used to start solr. It has all solr configurations for standalone mode (not cloud)
*    Starting Solr: Modify and use scripts/solr_5_standalone_start.bat